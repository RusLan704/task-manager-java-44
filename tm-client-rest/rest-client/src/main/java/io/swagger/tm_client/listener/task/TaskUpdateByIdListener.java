package io.swagger.tm_client.listener.task;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;
import io.swagger.tm_client.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = api.findById_0(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final TaskDTO newTask = new TaskDTO();
        newTask.setId(id);
        newTask.setName(name);
        newTask.setDescription(description);
        TaskDTO taskUpdated = api.updateTaskById(newTask);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}