package io.swagger.tm_client.exception.user;

import org.jetbrains.annotations.NotNull;
import io.swagger.tm_client.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    @NotNull
    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}