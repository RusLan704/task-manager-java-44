package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.UserDTO;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class AuthRestTest {

    private final DefaultApi api = new DefaultApi();

    @Test
    public void testLogin() throws ApiException {
        Assert.assertTrue(api.login("test", "test"));
        Assert.assertFalse(api.login("false", "false"));
    }

    @Test
    public void testLogout() throws ApiException {
        Assert.assertTrue(api.login("test", "test"));
        @Nullable final UserDTO user = api.profile();
        Assert.assertEquals("test", user.getLogin());
    }

}
