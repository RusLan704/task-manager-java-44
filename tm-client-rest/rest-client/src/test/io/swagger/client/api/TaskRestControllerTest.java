package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProjectDTO;
import io.swagger.client.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TaskRestControllerTest {

    private final DefaultApi api = new DefaultApi();

    @Before
    public void initData() throws ApiException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId("project-id");
        project.setName("test");
        project.setDescription("test");
        api.login("admin", "admin");
        api.create(project);
    }

    @After
    public void clearData() throws ApiException {
        api.removeOneById_0("project-id");
        api.logout();
    }

    @Test
    public void findAll() throws ApiException {
        Assert.assertTrue(api.findAll_0().size() > 0);
    }

    @Test
    public void testCreate() throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setId("test");
        task.setName("test");
        task.setDescription("test");
        task.setProjectId("project-id");
        api.create_0(task);
        @Nullable final TaskDTO projectUpdated = api.findById_0("test");
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getName(), "test");
        Assert.assertEquals(projectUpdated.getDescription(), "test");
        api.removeOneById_0("test");
    }

    @Test
    public void testRemove() throws ApiException {
        final TaskDTO task = new TaskDTO();
        task.setId("test");
        task.setName("test");
        task.setDescription("test");
        task.setProjectId("project-id");
        api.create_0(task);
        api.removeOneById_0("test");
        Assert.assertFalse(api.findAll().contains(task));
    }

    @Test
    public void testUpdate() throws ApiException {
        final TaskDTO task = new TaskDTO();
        task.setId("test");
        task.setName("name");
        task.setDescription("description");
        task.setProjectId("project-id");
        api.create_0(task);
        task.setName("new name");
        task.setDescription("new description");
        api.updateTaskById(task);
        @Nullable final TaskDTO taskUpdated = api.findById_0("test");
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(taskUpdated.getName(), "new name");
        Assert.assertEquals(taskUpdated.getDescription(), "new description");
        api.removeOneById_0("test");
    }
    
}
