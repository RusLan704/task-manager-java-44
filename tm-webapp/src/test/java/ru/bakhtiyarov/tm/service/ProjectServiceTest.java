package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @NotNull
    private Project project;

    @NotNull
    private User user;

    @Before
    public void initData() {
        user = new User("login", "pass", Role.USER);
        userService.save(user);
        project = new Project("name", "description");
        project.setUser(user);
        projectService.save(project);
    }

    @After
    public void deleteData() {
        projectService.removeAll();
        userService.removeAll();
    }

    @Test
    public void testCreate() {
        Assert.assertEquals(1, projectService.findAll(user.getId()).size());
        @Nullable final Project project = new Project("new name", "new description");
        project.setUser(user);
        projectService.create(user.getId(), project);
        Assert.assertEquals(2, projectService.findAll(user.getId()).size());
        @Nullable final Project projectAdded = projectService.findById(project.getId());
        Assert.assertEquals(project.getId(), projectAdded.getId());
        Assert.assertEquals(project.getName(), "new name");
        Assert.assertEquals(project.getDescription(), "new description");
    }

    @Test
    public void testUpdateProjectById() {
        Assert.assertEquals(1, projectService.findAll(user.getId()).size());
        @Nullable final Project project1 = new Project("new name", "new description");
        project1.setId(project.getId());
        project1.setUser(user);
        projectService.updateProjectById(user.getId(), project1);
        @Nullable final Project projectUpdated = projectService.findById(project.getId());
        Assert.assertEquals(projectUpdated.getId(), project.getId());
        Assert.assertEquals(projectUpdated.getName(), "new name");
        Assert.assertEquals(projectUpdated.getDescription(), "new description");
    }

    @Test
    public void testFindAllByUserId() {
        Assert.assertEquals(1, projectService.findAll(user.getId()).size());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void testFindByUserIdAndId() {
        @Nullable final Project testProject = projectService.findOneById(user.getId(), project.getId());
        Assert.assertEquals(testProject.getId(), project.getId());
        Assert.assertEquals(testProject.getName(), project.getName());
        Assert.assertEquals(testProject.getDescription(), project.getDescription());
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @Nullable final Project testProject = projectService.findOneByIndex(user.getId(), 0);
        Assert.assertEquals(testProject.getId(), project.getId());
        Assert.assertEquals(testProject.getName(), project.getName());
        Assert.assertEquals(testProject.getDescription(), project.getDescription());
    }

    @Test
    public void testFindByUserIdAndName() {
        @Nullable final Project testProject = projectService.findOneByName(user.getId(), project.getName());
        Assert.assertEquals(testProject.getId(), project.getId());
        Assert.assertEquals(testProject.getName(), project.getName());
        Assert.assertEquals(testProject.getDescription(), project.getDescription());
    }

    @Test
    public void testFindById() {
        @Nullable final Project testProject = projectService.findById(project.getId());
        Assert.assertEquals(testProject.getId(), project.getId());
        Assert.assertEquals(testProject.getName(), project.getName());
        Assert.assertEquals(testProject.getDescription(), project.getDescription());
    }

    @Test
    public void testRemoveAll() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeAll();
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testRemoveAllByUserId() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeAll(user.getId());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndIndex() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeOneByIndex(user.getId(), 0);
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndName() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeOneByName(user.getId(), project.getName());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testRemoveOneById() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeOneById(project.getId());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndId() {
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeOneById(user.getId(), project.getId());
        Assert.assertEquals(0, projectService.findAll().size());
    }

}