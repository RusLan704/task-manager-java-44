package ru.bakhtiyarov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.repository.IProjectRepository;
import ru.bakhtiyarov.tm.repository.ITaskRepository;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectRestEndpointTest {

    @NotNull
    private final String baseUrl = "/api/rest/project";

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @Before
    @Transactional
    public void initData() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        @Nullable final User user = new User("login", passwordEncoder.encode("pass"), Role.USER);
        userRepository.save(user);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void createTest() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        final String requestJson = ow.writeValueAsString(project);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(baseUrl + "/create")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").value(project.getId()))
                .andExpect(jsonPath("$.name").value(project.getName()))
                .andExpect(jsonPath("$.description").value(project.getDescription()));
    }

    @Test
    public void findAllTest() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders
                        .get(baseUrl + "/findAll")
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..id", hasItem(project.getId())))
                .andExpect(jsonPath("$..name", hasItem(project.getName())))
                .andExpect(jsonPath("$..description", hasItem(project.getDescription())));
    }

    @Test
    public void findByIdTest() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders
                        .get(baseUrl + "/findById/{id}", project.getId()
                )
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(project.getId()));
    }

    @Test
    public void testUpdateProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName("new name");
        projectDTO.setDescription("new description");

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(projectDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/updateById")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(project.getId()))
                .andExpect(jsonPath("$.name").value("new name"))
                .andExpect(jsonPath("$.description").value("new description"));
    }

    @Test
    public void removeByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/removeById/{id}", project.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        @Nullable final Project projectDeleted = projectRepository.findById(project.getId()).orElse(null);
        Assert.assertNull(projectDeleted);
    }

}