package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class AuthRestEndpointTest {

    @NotNull
    private final String baseUrl = "/api/rest/authentication";

    @NotNull
    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private User user;

    @Before
    public void initData() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        user = new User("login", passwordEncoder.encode("pass"), Role.USER);
        userRepository.save(user);
    }

    @After
    public void deleteData() {
        userRepository.deleteAll();
    }

    @Test
    public void testLogin() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(baseUrl + "/login")
                                .param("username", "login")
                                .param("password", "pass"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    public void testProfile() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(baseUrl + "/login")
                                .param("username", "login")
                                .param("password", "pass"));
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(baseUrl + "/profile"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.login").value("login"));
    }

    @Test
    public void testLogout() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(baseUrl + "/login")
                                .param("username", "login")
                                .param("password", "pass"));
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(baseUrl + "/logout"))
                .andExpect(status().isOk());
    }

}
