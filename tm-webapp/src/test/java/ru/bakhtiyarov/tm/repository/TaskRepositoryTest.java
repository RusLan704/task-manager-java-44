package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class TaskRepositoryTest {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @NotNull
    private User user;

    @Before
    @Transactional
    public void initData() {
        user = new User("login", "pass", Role.USER);
        userRepository.save(user);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void testSave() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @NotNull final Task task = new Task("name2", "description2");
        taskRepository.save(task);
        Assert.assertEquals(2, taskRepository.findAll().size());
        @Nullable final Task taskAdded = taskRepository.getOne(task.getId());
        Assert.assertNotNull(taskAdded);
        Assert.assertEquals(task.getId(), taskAdded.getId());
    }

    @Test
    public void testFindByUserIdAndName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @Nullable final Task taskTest = taskRepository.findByUserIdAndName(user.getId(), task.getName());
        Assert.assertNotNull(taskTest);
        Assert.assertEquals(user.getId(), taskTest.getUser().getId());
        Assert.assertEquals(task.getId(), taskTest.getId());
        Assert.assertEquals(task.getName(), taskTest.getName());
        Assert.assertEquals(task.getDescription(), taskTest.getDescription());
    }

    @Test
    public void testFindByUserIdAndId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @Nullable final Task taskTest = taskRepository.findByUserIdAndId(user.getId(), task.getId());
        Assert.assertNotNull(taskTest);
        Assert.assertEquals(user.getId(), taskTest.getUser().getId());
        Assert.assertEquals(task.getId(), taskTest.getId());
        Assert.assertEquals(task.getName(), taskTest.getName());
        Assert.assertEquals(task.getDescription(), taskTest.getDescription());
    }

    @Test
    public void testFindAllByUserId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        @Nullable final List<Task> tasks = taskRepository.findAllByUserId(task.getUser().getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(5, taskRepository.findAll().size());
    }

    @Test
    public void testFindAllByUserIdAndProjectId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @Nullable final List<Task> tasks = taskRepository.findAllByUserIdAndProjectId(
                task.getUser().getId(),
                task.getProject().getId()
        );
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndName(task.getUser().getId(), task.getName());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndId(task.getUser().getId(), task.getId());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testDeleteAllByUserId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @NotNull final Task task = new Task();
        task.setUser(user);
        taskRepository.save(task);
        Assert.assertEquals(2, taskRepository.findAll().size());
        @Nullable final List<Task> tasks = taskRepository.deleteAllByUserId(task.getUser().getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testDeleteAllByUserIdAndProjectId() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);
        @Nullable final List<Task> tasks = taskRepository.deleteAllByUserIdAndProjectId(
                task.getUser().getId(),
                task.getProject().getId()
        );
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

}