package ru.bakhtiyarov.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.repository.IProjectRepository;
import ru.bakhtiyarov.tm.repository.ITaskRepository;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private Project project;

    @NotNull
    private final String baseUrl = "/projects";

    @Before
    @Transactional
    public void initData() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        @Nullable final User user = new User("login", passwordEncoder.encode("pass"), Role.USER);
        userRepository.save(user);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        @Nullable final Task task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testList() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project/project-list"));
    }

    @Test
    public void testCreateGet() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project/project-create"));
    }

    @Test
    public void testCreatePost() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("test");
        projectDTO.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create").flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/list"));
    }

    @Test
    public void testRemove() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/remove/{id}", project.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/list"));
    }

    @Test
    public void testGetUpdate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", project.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project/project-update"));
    }

    @Test
    public void testPostUpdate() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("test");
        projectDTO.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", project.getId()).flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/list"));
    }

}