package ru.bakhtiyarov.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.bakhtiyarov.tm.dto.CustomUser;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;

public class UserUtil {

    static public String getAuthUserId() throws AccessDeniedException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) throw new AccessDeniedException();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException();
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
