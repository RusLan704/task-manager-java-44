package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    List<Task> deleteAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}