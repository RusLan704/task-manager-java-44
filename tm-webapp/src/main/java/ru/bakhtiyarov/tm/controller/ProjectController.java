package ru.bakhtiyarov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.CustomUser;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.enumeration.Status;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService,
            @NotNull IProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.projectConverter = projectConverter;
    }

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/list")
    public ModelAndView list(@AuthenticationPrincipal final CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-list");
        List<ProjectDTO> projects = projectService
                .findAll(user.getUserId())
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-create");
        modelAndView.addObject("project", new ProjectDTO());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(
            @AuthenticationPrincipal CustomUser user,
            @ModelAttribute("project") ProjectDTO project
    ) {
        projectService.create(user.getUserId(), projectConverter.toEntity(project));
        return "redirect:/projects/list";
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable(value = "id") @NotNull final String id
    ) {
        projectService.removeOneById(user.getUserId(), id);
        return new ModelAndView("redirect:/projects/list");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-update");
        @Nullable final Project project = projectService.findOneById(user.getUserId(), id);
        modelAndView.addObject("project", projectConverter.toDTO(project));
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(
            @AuthenticationPrincipal CustomUser user,
            @ModelAttribute("project") ProjectDTO project
    ) {
        projectService.updateProjectById(user.getUserId(), projectConverter.toEntity(project));
        return "redirect:/projects/list";
    }

}