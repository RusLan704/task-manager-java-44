package ru.bakhtiyarov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class SessionNotFoundException extends AbstractException {

    @NotNull
    public SessionNotFoundException() {
        super("Error! Session Not found...");
    }

}
