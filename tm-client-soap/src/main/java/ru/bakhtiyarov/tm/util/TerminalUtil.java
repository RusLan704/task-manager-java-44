package ru.bakhtiyarov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}
