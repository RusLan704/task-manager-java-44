package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.TaskDTO;
import ru.bakhtiyarov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @NotNull
    @Autowired
    private TaskSoapEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ProjectSoapEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.findProjectById(projectId);
        if(project == null){
            System.out.println("[FAIL]");
            return;
        }
        @NotNull final TaskDTO task = new TaskDTO();
        task.setProjectId(projectId);
        task.setName(taskName);
        task.setDescription(description);
        taskEndpoint.createTask(task);
        System.out.println("[OK]");
    }

}
