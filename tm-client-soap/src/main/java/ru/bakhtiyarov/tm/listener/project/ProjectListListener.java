package ru.bakhtiyarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @NotNull
    @Autowired
    private ProjectSoapEndpoint projectEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @EventListener(condition = "@projectListListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST PROJECT]");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects();
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}